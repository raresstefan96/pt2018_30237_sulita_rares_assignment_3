public void displayDetails(Car c)
	{
		manufacturerTextField.setText(c.getManufacturer());
		yearTextField.setText(Integer.toString(c.getYear()));
		modelTextField.setText(c.getModel());
		priceTextField.setText(Integer.toString(c.getPrice()));
		kmTextField.setText(Double.toString(c.getKilometers()));
		infoTextArea.setText(c.getInformation());
	}

	public String getInfoText()
	{
		return infoTextArea.getText();
	}

	public String getKmText()
	{
		return kmTextField.getText();
	}

	public String getManufacturerText()
	{
		return manufacturerTextField.getText();
	}

	public String getModelText()
	{
		return modelTextField.getText();
	}

	public String getPriceText()
	{
		return priceTextField.getText();
	}

	public String getYearText()
	{
		return yearTextField.getText();
	}

	
	public void setFocusManufacturerTextField()
	{
		manufacturerTextField.grabFocus();
	}
}