//vertex position and UV coordinates
GLfloat vertexData[] = {
	// first triangle
	-5.0f, 0.0f, 0.0f, 0.0f, 0.0f,
	5.0f,0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 8.0f, 0.0f, 0.5f, 1.0f,
	// second triangle
	0.1f, 8.0f, 0.0f, 0.0f, 0.0f,
	5.1f, 0.0f, 0.0f, 0.0f, 0.0f,
	10.1f, 8.0f, 0.0f, 0.0f, 0.0f

};

GLuint vertexIndices[] = {
	0,1,2,
	3,4,5
};

float delta = 0;
float movementSpeed = 2; // units per second
void updateDelta(double elapsedSeconds) {
	delta = delta + movementSpeed * elapsedSeconds;
}
double lastTimeStamp = glfwGetTime();
void renderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.8, 0.8, 0.8, 1.0);
	glViewport(0, 0, retina_width, retina_height);

	//initialize the model matrix
	model = glm::mat4(1.0f);
	modelLoc = glGetUniformLocation(myCustomShader.shaderProgram, "model");

	processMovement();

	myCustomShader.useShaderProgram();

	//initialize the view matrix
	glm::mat4 view = myCamera.getViewMatrix();
	//send matrix data to shader
	GLint viewLoc = glGetUniformLocation(myCustomShader.shaderProgram, "view");
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

	// get current time
	double currentTimeStamp = glfwGetTime();
	updateDelta(currentTimeStamp - lastTimeStamp);
	lastTimeStamp = currentTimeStamp;

	delta += 0.001;
	model = glm::translate(model, glm::vec3(delta, 0, 0));

	//create rotation matrix
	model = glm::rotate(model, angle, glm::vec3(0, 1, 0));
	model = glm::rotate(model, 3.14f, glm::vec3(0, 1, 0));
	//send matrix data to vertex shader
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	//glBindTexture(GL_TEXTURE_2D, texture); 
	for (int i = 0; i < 500; i++)
		myModel.Draw(myCustomShader);
	

	glActiveTexture(GL_TEXTURE0);
	glUniform1i(glGetUniformLocation(myCustomShader.shaderProgram, "diffuseTexture"), 0);
	glBindTexture(GL_TEXTURE_2D, texture);


	//glBindVertexArray(objectVAO);
	//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

}